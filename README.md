ics-ans-role-check-mk-server
===================

Ansible role to install check-mk-server.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
#Set omd instance
omd_instance: initop
#Enable/disable omd instance configuration from a backup
omd_config_instance: false
omd_path_backup:
checkmk: https://mathias-kettner.de/support/1.5.0p2/check-mk-raw-1.5.0p2-el7-38.x86_64.rpm
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-check-mk-server
```

License
-------

BSD 2-clause
